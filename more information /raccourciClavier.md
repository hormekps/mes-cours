# Raccourci Clavier

`ctrl + A` = selectionner tout le contenu

`ctrl + F` = faire une recherche

`cmd + R` = rafraîchir la page

`ctrl + tab` = basculer sur l'onglet suivant

`ctrl + shift` + tab = revenir sur l'onglet précédent

`shift + flèche` = sélectionner texte

`shift + ctrl + flèche` = sélectionner un mot

`cmd + z` = revenir en arrière 

`shift + ctrl + z` = annuler le retour en arrière

`cmd + d` = dupliquer

`alt + j` = sélectionner une occurrence 

`ctrl + y` = supprimer une ligne 

`ctrl + x` = couper une ligne

`alt + ctrl + l` = remettre en forme

`ctrl + espace` = Voir les choix possibles

`ctrl + s` = enregistrer

`shift + ctrl + flèche haut ou bas` = Déplacer la ligne

`shift + f6` = changer le nom partout

`ctrl + -` = réduire la ligne

`alt + cmd + a` = rajouter dossier sur git 

`shif + k` = push sur git 

`cmd + entre `  = voir ou ce trouver la variable 

`shift + cmd + 4` = capture d'ecran choisie

`shift + cmd + 3` = capture de tous l'ecran 

`shift + fleche droit ou gauche` = selection le mots ou texte 

`shift + fleche haut ou bas` = selection toute mots ou texte 
