# ignore des module 

## fiche pas utile 
* .idea
* cela commence par un point 
* les package 

## supprime completement un dossier
* git rm -rf .idea....

## Les étapes

1. Créer un fichier dans la racine appelé `.gitignore` puis l'ajouter a git (.nodemodule par exemple)
2. Dans ce fichier, mettre tous les fichiers et dossiers que l'on ne veut pas en commencant par un .
3. Pour retirer un fichier (idea par exemple) qui n'a pas été ignoré, Saisir `git rm -rf .idea`
4. On refait commit, idea aura disparu


monfichier/*.txt
