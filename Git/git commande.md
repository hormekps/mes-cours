# Commande git

## git status

On voit qu'on est sur la branche master, qu'il n'y a aucun commit etc
On a la liste des fichiers non suivi également.

## git add

Le fichier sera pris en compte par git mais pas envoyer.

`git add index.html`

## git commit

``git commit -m "first commit"``

## git ls-files --stage

Permet d'obtenir des informations sur les fichiers suivi par git 

## git diff

Permet d'avoir un apercu de ce qu'il va se passer si on add.

## git diff --staged

Compare le zone de transit (index) avec le dernier commit

## rm -rf .git

Supprime completement git il faudra faire git init pour le faire fonctionner a nouveau.

## rm .gitignore

supprime gitignore et le met en attente

ensuite il faut faire un git add .gitignore puis on commit

## git log

permet de voir l'historique des commit

## git blame

permet de savoir qui a modifié quoi


