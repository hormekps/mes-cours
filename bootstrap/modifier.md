# Modifier bootstrap

> Règle : Ne jamais modifier des fichiers vendors

## Modifier les propriétés bootstrap en ajoutant une classe au container

1. Créer un fichier dans le dossier CSS appelé bootstrap-mod.css par exemple
2. Faire un import de ce fichier dans le fichier styles.css
3. Lier le styles.css dans le fichier html
4. Rajouter une classe my-container au container
5. Apporter les modifications voulues sur le fichier mod.bootstrap.css à l'aide de la classe créée

## Modifier bootstrap directement dans le container

1. Créer un fichier dans le dossier CSS appelé bootstrap-mod.css par exemple
2. Faire un import de ce fichier dans le fichier styles.css
3. Lier le styles.css dans le fichier html
4. Modifier .container sans créer la classe my-container
5. Apporter les modifications voulu sur le fichier mod.bootstrap.css à l'aide de .container directement

Pour modifier le width d'une col qui est déja fixé par le fichier bootstrap, il faut `mettre les liens css après le lien bootstrap et mettre son propre fichier css avant`.
