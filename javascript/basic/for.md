# Boucle For

````javascript
for (let i = 0, i <= 20, i = i + 1 ) {
    console.log('hormeli');
}
````

1. On initialise la boucle a 0
2. La condition de continuation (tant qu'elle est vrais la boucle continue)
3. On veut que i aille de 1 en 1

La console va renvoyer 20 fois 'hormeli';

On peut générer des tableaux avec la boucle for.
