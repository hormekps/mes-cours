# Destructuration des arrays

Il s'agit de récupérer les variables dans un tableau.

## Ancienne méthode pour manipuler un élément d'un tableau

````javascript
const tab = ['tata', 'titi', 'toto', 'tutu'];

tab[1] = 'coucou';
console.log(tab);
````

Dans cet exemple, nous avons modifié `titi `par `coucou`.

## Nouvelle méthode (la destructuration)

````javascript
const tab = ['tata', 'titi', 'toto', 'tutu'];

const [var1, var2, var3, var4] = tab;

console.log(var3)
````
La console devrait renvoyer toto. Si on met des virgules et qu'on laisse seulement le var3, ça marche aussi `const [,, var3,] = tab;`
C'est une façon plus simple de récupérer et manipuler des données à l'interieur des tableaux.

