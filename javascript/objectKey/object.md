# Les objets

## Destructuration des objets

Il s'agit de manipuler le objets plus facilement.

### Ancienne méthode

````javascript
const user = {
    firstName: 'horm",
    lastName: 'LeBoss',
    age: 34,
    billionaire: true,
    eyes: 'blue'
};

const age = user.age;
const lastName = user.lastName;

console.log(age, lastName);
````

La console va renvoyer l'age et le nom.

### Nouvelle méthode

````javascript
const user = {
    firstName: 'horm",
    lastName: 'LeBoss',
    age: 34,
    billionaire: true,
    eyes: 'blue'
};

const {age, lastName} = user;

console.log(age, lastName);
````

### Rajouter des attributs a un objet
````javascript
const user = {
    firstName: 'horm",
    lastName: 'LeBoss',
    age: 34,
    billionaire: true,
    eyes: 'blue'
};

`user.adress = 2 rue des fleurs` 
````
On peut aussi rajouter l'adresse directement dans l'objet à la suite.

### Avoir accès a un attributs inconnu

C'est une notation qui permet d'avoir accès a l'attribut d'un objet quand on ne sait pas le nom de cet attribut a l'avance.

````javascript
const attrs = ['lastName', 'age'];

attrs.forEach(attr => console.log(user[attr]))
````
l'élément entre les crochets n'existe pas, il va aller récuperer ce qu'on demande dans la const attrs.
Il va aller chercher lastName d'abord, puis l'âge et les afficher un par un.
