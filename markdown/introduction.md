# Introduction au markdown

## titre :  
````markdown
#Titre
````
## Sous titre : 
````markdown
##Sous titre
````
## Mettre un petit bout de code : 
`code`

## Mettre du code : 

```` markdown
const toto = 42
````

## Mettre en gras :

**GRAS**

## Mettre en italique :

_ITALIQUE_

## Mettre une citation :

> CITATION

## Mettre une citation dans une citation :

>>CITATION

## Mettre un lien :

[Salut](www.salut.com)

## Liste à puces :

* puce1
* puce2

## Liste à puces numérotées :

1. puce1
2. puce2

## Mettre en rouge

`salut`
