# organiser des feuille de css

Pour organiser le CSS il est possible de créer plusieurs fichier CSS

## regle generale decouper les feuille selon le site


*faire un dossier css et mettre les style dedans
* cree un ficher style

## cree une liason

* cree un stylesheet
```css
@import url ('main menu')
```

## Etapes

1. Aller dans assets et créer un dossier CSS
2. Deplacer le style.css dans le nouveau dossier CSS
3. Cliquer sur "Search for references" et cliquer sur ok
4. Créer un nouveau fichier CSS dans le dossier CSS avec un nom qui fait référence a la page HTML (ici main.html)
5. Copier le code css voulu 
6. Retourner sur style.css et saisir `@import url('main.css');`
7. Le fichier style.css ne devra comporter que des imports de fichiers css
